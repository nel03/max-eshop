const express = require('express');

const userRoutes = require('./routes/user.routes')
const productRoutes = require('./routes/product.routes')
const cartRoutes = require('../routes/cart.routes')
const db = require('./data/database')



const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use(userRoutes)
app.use('/product', productRoutes)
app.use('/cart', cartRoutes)


const port = 3000;

app.listen(port, () => console.log(`API is now online at port: ${port}`))