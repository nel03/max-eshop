const mongoose = require('mongoose');

mongoose.connect(``, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const dbConnect = mongoose.connect;

dbConnect.on('error', () => console.error('Connection Error'));
dbConnect.once('open', () => console.log('Connected to MongoDB'));

module.exports = dbConnect;