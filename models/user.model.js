const mongoose = require('mongoose');

const userSchema = {
    firstName: {
        type: String,
        required: [true, "First Name required"]
      },
      lastName: {
        type: String,
        required: [true, "Last Name required"]
      },
      email: {
        type: String,
        required: [true, "Email required"]
      },
      password: {
        type: String,
        required: [true, "Password required"]
      },
      address: [
        {
            address: {
                type: String,
                required: [true, "Address required"]
            },
            street: {
                type: String,
                required: [true, "Street required"]
            },
            city: {
                type: String,
                required: [true, "City required"]
            },
            country: {
                type: String,
                required: [true, "Country required"]
            }
        }
      ],
      isAdmin: {
        type: Boolean,
        default: false,
      },
      mobileNo: {
        type: String,
        required: [true, "Mobile No. required"]
      },
    
}

module.exports = mongoose.model("User", userSchema);