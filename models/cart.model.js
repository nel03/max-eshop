

class Cart {
    constructor(items =[], totalQuantity = 0, totalPrice = 0){
        this.item = items;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice
    }

    addItem(product){
        const cartItem ={
            product: product,
            quantity: 1,
            totalPrice: product.price
        }

        for(let i =0; i < this.items.length; i++){
            const item = this.item[i]
            if(item.product.id === product.id){
                cartItem.quantity = cartItem.quantity + 1
                cartItem.totalPrice = cartItem.totalPrice + product.price
                this.item[i] = cartItem
                return;
            }
        }

        this.item.push(product)
    }
}