const mongoose = require('mongoose');

const productSchema =  new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name of Product is required"]
    },
    image:{
        type: String,
        required: [true, "Image is required"]
    },
    price: {
        type: Number,
        required: [true, "Price of Product is required"]
    },
    stock: {
        type: Number,
        required: [true, "Stock of Product is required"]
    },
    isAvailable: {
        type: Boolean,
        default: true,
    },
    size: {
        type: String,
        required: [true, "Size of Product is required"]
    },
    description: {
        type: String,
        required: [true, "description of Product is required"]
    },
    buyers: [
        {
            userId: {
                type: String,
        required: [true, "User Id is required"]
            },
            registeredOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Product", productSchema);