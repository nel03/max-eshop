const User = require('../models/user.model')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Register New User
async function signup(req, res) {
    const hashedPassword = bcrypt.hashSync(req.body.password, 10)
    try {
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hashedPassword,
            address: req.body.address,
            street: req.body.street,
            city: req.body.city,
            country: req.body.country
        })
        const result = await newUser.save();
        if(result){
            return res.send(true)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
}

// Login User
async function login(req, res){
    try{
        const user = await User.findOne({email: req.body.email});
        if(user == null){
            return res.send(false)
        }
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password)
        if(isPasswordCorrect){
            return res.send({access: auth.createAccessToken(user)})
        }
        return res.send(false);
    }catch(error){
        console.log(error);
        return res.send(error)
    }
}

module.exports = {
    checkEmailExists: checkEmailExists, //TODO: create an email checker
    signup: signup,
    login: login,
    //TODO: create admin interface
    checkUserProfile: checkUserProfile // admin function
}