const Product = require('../models/product.model')

async function addProduct (req, res){
    const userData = auth.decode(req.headers.authorization);
    console.log(userData)
    if(!userData.isAdmin){
        return res.send("You are not an admin. You are not allowed to Add Product")
    }
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        stock: req.body.stock
    })
    try{
        const product = await newProduct.save()
        if(product){
            return res.send(`Product ${req.body.name} is added`)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function getAllProducts(req, res){
    try{
        const product = await product.find({})
        if(product){
            return res.send(Product)
        }
        return res.sed(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function getTheProduct(req, res){
    try{
        const product = await Product.findById(req.params.productId)
        if(product){
            return res.send(product)
        }
        return res.send(false)
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

async function getAllActive(req, res){
    try{
        const products = await Product.find({isAvailable: true})
        if(products && products.length > 0){
            return res.send(products)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};

async function updateTheProduct(req, res){
    const userData = auth.decode(req.headers.authorization)

    const updatedProduct = {
        price: req.body.price,
        stock: req.body.stock
    }
    try{
        if(!userData.isAdmin){
            return res.send("You are not an admin! You are not required to update a product")
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, updateProduct)
        if(result){
            return res.send(updatedProduct)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
}

async function archiveProduct(req, res){
    const userData = auth.decode(req.headers.authorization)
    try{
        if(!userData.isAdmin){
            return res.send("You are not an admin! you are not required to archive a Product")
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, {isActive:req.body.isActive})

        if(result){
            return res.send(true)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};


module.exports = {
    addProduct: addProduct,
    getAllProducts: getAllProducts,
    getProduct: getTheProduct,
    getAllActive: getAllActive,
    updateTheProduct: updateTheProduct,
    archiveProduct: archiveProduct
}