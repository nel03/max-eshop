const express = ('express');
const userController = require('../controllers/user.controller')
const auth = require('../auth');

const router = express.Router();

router.get('/', userController.checkEmailExists) // Check if Email already exists
router.post('/signup', userController.signup) // create new Account
router.post('/login', userController.login) // Login Account
router.get('/details', ) //TODO: Admin checks the user details



module.exports = router