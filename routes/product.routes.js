const express = require('express');

const productController = ('../controllers/product.controller')

const router = express.Router();

//admin
router.post('/', auth.verify, productController.addProduct ) // Add new product
router.get('/all', auth.verify,productController.getAllProducts) // list all products active or not active
router.put('/:productId', auth.verify,productController.updateTheProduct) //update the product
router.put('/archive', auth.verify, productController.archiveProduct) // archive a product that is not active

//user
router.get('/', productController.getAllActive) // list all active products
router.get('/:productName', productController.getTheProduct) // show Specific Product



module.exports = router